<?php
/* Challenge one For Jobspire */
// code for a function (or method) that takes a variable name as parameter.
// Generate a random number between 6 and 15 and return the result in this format: “Name GeneratedNumber”. Eg: “Rahul 11”

	function randomNameNum($name) {
		return $name." ".rand(6, 15); 
	}
	echo randomNameNum('Rahul');

?>
